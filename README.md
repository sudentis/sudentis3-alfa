# APP SUDENTIS 3.0 - En desarrollo :hammer:

Aplicación en desarrollo para la gestión de clinicas odontologicas en linea, se puede correr en Windows y Linux, solamente seguir las instrucciones con cuidado, dudas solo escribanme.

## Prerequisitos 

**Backend - Servidor**
* [Miniconda](https://docs.conda.io/en/latest/miniconda.html) - Para crear entornos virtuales, recomiendo instalar Miniconda Windows o Linux con Python 3.7
* [Django](https://www.djangoproject.com/) - El Framework principal para el Backend
* [Graphene-Django](https://docs.graphene-python.org/projects/django/en/latest/) - Genera las APIs con GRAPHQL
* [Django-filter](https://django-filter.readthedocs.io/en/master/) - Para mejorar las consultas de modelos
* [Django-graphql-jwt](https://django-graphql-jwt.domake.io/en/stable/index.html) - Para trabajar con tokens, usuarios e inicio de sesion.
* [Django-cors-headers](https://github.com/ottoyiu/django-cors-headers) - Para trabajar con CORS, parece que Django no admite CORS por defecto.
```
## Instalar miniconda y abrir el prompt
## crear un entorno virtual con python 
## Para Windows
usuario@joel:~/home$ conda create -n joe-django python
## activar el entorno virtual joe-django
usuario@joel:~/home$ source activate joe-django python ##---> para linux
C:\home>conda activate joe-django python  ##---> para windows
## Instalar las herramientas 
(joe-django) usuario@joel:~/home$ pip install Django
(joe-django) usuario@joel:~/home$ pip install graphene_django
(joe-django) usuario@joel:~/home$ pip install django-filter
(joe-django) usuario@joel:~/home$ pip install django-graphql-jwt
(joe-django) usuario@joel:~/home$ pip install django-cors-headers
```

**Frontend - Cliente**
* [Node](https://nodejs.org/es/) - para instalar Vuejs
* [NPM](https://www.npmjs.com/) - para instalar Packages
* [Axios](https://github.com/axios/axios) - para conectar las apis
* [Graphql](https://graphql.org/learn/) - para conectar mejor las APIs
* [Graphql-tag](https://github.com/apollographql/graphql-tag) - ayuda a Graphql
* [Vuex](https://vuex.vuejs.org/) - para trabajar con el cache del frontend
* [Template Vuejs Admin Free de COREUI](https://github.com/coreui/coreui-free-vue-admin-template) - plantilla gratis de Admin escrita en Vuejs

```
## Instalar Node y NPM, para verificar usa lo siguiente
usuario@joel:~/home$ node -v
v8.12.0
usuario@joel:~/home$ npm -v
6.4.1
```

## Complementos
* [Visual Studio Code](https://code.visualstudio.com/) - Es un editor de texto que tiene un terminal integrado y paquetes que sirven al momento de escribir codigo, recomiendo usarlo e instalar los paquetes de vuejs y python.
* [Bootstrap Vuejs](https://bootstrap-vue.js.org/) - Para las plantillas html.
* [Gitlab](https://about.gitlab.com/) - Para tener un respaldo del codigo en la nube, recomiendo crear una cuenta.
* [Git](https://git-scm.com/) - Es un sistema de control de versiones ayuda a no tener muchas copias del mismo codigo, necesario para trabajar con Gitlab y creo que esta incluido en Visual Studio Code, recomiendo instalar o verificar si ya lo tienen.


## Para Empezar 

La aplicacion funciona con dos herramientas:

* **Backend-Django**, funciona con Django, debes correr en un entorno conda que tenga los prerequisitos instalados.
```
## Primero debes entrar a la carpeta Backend y correr 
## en un entorno virtual de django 
## Corriendo las migraciones, para la base de datos
(joe-django) usuario@joel:~/Backend$ python manage.py makemigrations
(joe-django) usuario@joel:~/Backend$ python manage.py migration
(joe-django) usuario@joel:~/Backend$ python manage.py createsuperuser
## agregar un usuario, correo y contraseña
## crear el superusuario para agregar los datos y corre
(joe-django) usuario@joel:~/Backend$ python manage.py runserver

System check identified no issues (0 silenced).
March 01, 2019 - 09:47:04
Django version 2.1.5, using settings 'backend_django21.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.

## ir al host http://127.0.0.1:8000/admin para verificar que este corriendo
## introduce tu usuario y contraseña que creaste anteriormente
## te redirige a la pantalla de administrador de django donde puedes crear
## usuarios, clinicas y demas.
```
* **Frontend-React**, Esta escrito en Vuejs ya esta integrado con django, es mas facil que React y se entiende mas rapido, debes tener instalado node y npm.
```
## entra a la carpeta Frontend y carga los paquetes
usuario@joel:~/Backend$ npm install
## se instalar los paquetes de COREUI, luego instalar los paquetes restantes
usuario@joel:~/Backend$ npm install axios --save
usuario@joel:~/Backend$ npm install graphql --save
usuario@joel:~/Backend$ npm install graphql-tag --save
usuario@joel:~/Backend$ npm install vuex --save
## corre la aplicacion
usuario@joel:~/Backend$ npm run serve

 DONE  Compiled successfully in 30675ms                                                         04:47:24


  App running at:
  - Local:   http://localhost:8080/
  - Network: http://10.10.75.87:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.

## ir al host http://127.0.0.1:8080 para verificar que este corriendo
## introduce un usuario y contraseña que creaste 
```
:warning: **Nota** Debe estar funcionando las dos aplicaciones al mismo tiempo pero en ventanas diferentes.

La conexión entre DJANGO y VUEJS se realiza a traves de APIS usando [Graphql](https://graphql.org/learn/) y [Graphene-Django](https://docs.graphene-python.org/projects/django/en/latest/).

## Usando Backend - Django Admin

Para el lado del servidor se esta conectando a travez del link [localhost:8000/admin](http://127.0.0.1:8000/admin), la interfaz de Django Admin sirve para agregar informacion a la base de datos del lado del servidor, sigue los siguientes pasos:
* Asegurate de haber creado un Super Usuario (python manage.py createsuperuser)
* Dirigete a [localhost:8000/admin](http://127.0.0.1:8000/admin/).
* Ingresa con tu Username y Password 
* Agrega la informacion de Usuarios, Dentistas, Clinicas, etc, la interfaz es sencilla de entender.

## Usando Frontend - Vuejs Admin COREUI

Para el lado del cliente se esta conectando a travez del link [localhost:8080](http://127.0.0.1:8080), que esta configurado para conectarse con el backend 
* Inicia Sesion con un usuario y contraseña creado en el Backend para entrar a todas las opciones disponibles.
* En la barra de navegacion de la izquierda estan todas las plantillas para armar el template.
* Las opciones marcadas con Joe azul, fueron creadas para la aplicacion Sudentis
* Las opciones marcadas con COREUI rojo, son de la plantilla 

**Nota** Para que la aplicacion funcione debes correr django y vuejs al mismo tiempo, esto abriendo y corriendo en dos ventanas diferentes


## Usando GRAPHQL

GRAPHQL es la manera como el Backend y Frontend se conectan, se han configurado las siguiente peticiones en GRAPHQL, estas se pueden realizan en el [localhost:8000/graphql](http://127.0.0.1:8000/graphql), la interfaz es sencilla de entender.

**Nota** Solo algunas peticiones se han configurado en el Frontend a manera de prueba, es necesario entedner GRAPHQL para elaborar las peticiones correctas en ambos lados de la palicacion.

### Crear usuario
Crea los usuarios
```
## Funciona en Graphql y Altair
mutation{
  createUser(
    username:"NOMBRE_USUARIO",
    email:"CORREO",
    password:"CONTRASEÑA"
  ){
	user{
        username
        email 
      }
  }
}
## Resultado despues de enviar SEND, solo funciona con POST.
{
  "data": {
    "createUser": {
      "user": {
        "username": "NOMBRE_USUARIO",
        "email": "CORREO"
      }
    }
  }
}
```
### Iniciar Sesion
Es algo raro porque en realidad no inicia, solo pide un token y con esto puedes entrar a los datos de tu usuario, hacer pedidos y solicitudes, investigar mas al respecto.
Para que funciones correctamente necesitas el [IDE Altair de Google](https://chrome.google.com/webstore/detail/altair-graphql-client/flnheeellpciglgpaodhkhmapeljopja/related) o el [IDE Rest Insomnia](https://insomnia.rest/), tambien se puede con el IDE Graphql, pero tienes que poner el header token en variables **Averiguar como**
```
## Funciona en Graphql y Altair
mutation{
  tokenAuth(
    username:"NOMBRE_USUARIO",
    password:"CONTRASEÑA"
  ){
    token
  }
}
## Resultado despues de enviar SEND, solo funciona con POST.
{
  "data": {
    "tokenAuth": {
      "token": "eyJ0eXAiO.........e7Ow6UKU20"
    }
  }
}
## Copiar el token y colocarlo en el encavezado (header) HTTP.
Header key: Authorization
Header value: JWT eyJ0eXAiO.........e7Ow6UKU20
## Y listo, este token sirve para autenticar al usuario y hacer sus pedido,
## el JWT se pone antes del token, no olvides de guardar, 
## para comprobar vamos a correr el "me"
## Funciona en Graphql y Altair
query{
  me{
    username
    email
  }
}
## Resultado despues de enviar SEND.
{
  "data": {
    "me": {
      "username": "NOMBRE_DE_USUARIO",
      "email": "CORREO"
    }
  }
}
```

### Crear dentista
Con el token que guardaste en el header vas a hacer lo siguiente, necesita que el usuario exista y tenga su token.
```
## Funciona en Graphql y Altair
mutation{
  createDentist(
    idDentist:DNI_O_COLEGIATURA_SIN_COMILLAS
  ){
    idDentist
  }
}
## Resultado despues de enviar SEND, ya se creo el dentista
{
  "data": {
    "createDentist": {
      "idDentist": DNI_O_COLEGIATURA_SIN_COMILLAS
    }
  }
}
```

### Crear clinica
Para crear la clinica se necesita: usuario, token, registrarse como dentista.
El token siempre en el header, de ahi se va jalar los datos del ususario y se va consultar si se registro como dentista
```
## Funciona en Graphql y Altair
mutation{
  createClinic(
    idRuc:RUC_SIN_COMILLAS,
    name:"NOMBRE_CLINICA"
  ){
    idRuc
    name
    username
    job
  }
}
## Resultado despues de enviar SEND, ya se creo la clinica
{
  "data": {
    "createClinic": {
      "idRuc": RUC_SIN_COMILLAS,
      "name": "NOMBRE_CLINICA",
      "username": "DENTISTA",
      "job": "Administrador"
    }
  }
}
```

### Consultar todos los usuarios
Se puede consultar todos los ususarios, pero solo se puede ver su Username y Email.
Para esto se uso only_fields en el class meta

```
## Funciona en Graphql y Altair
query{
  allUsers{
    username
    email
  }
}
## Resultado despues de enviar SEND, devuelve una lista con todos los ususarios
{
  "data": {
    "allUsers": [
      {
        "username": "joejhona",
        "email": "joejhona@gmail.com"
      },
      {
        "username": "dentista1",
        "email": ""
      },
      {
        "username": "dentistagra1",
        "email": "loco@gmail.com"
      }
    ]
  }
}
```

### Consultar todos las clinicas
Se puede consultar todas las clinicas, no tiene filtros asi que puedes jalar cualquier dato de la clinica.
```
## Funciona en Graphql y Altair
query{
  allClinics{
    idRuc
    name
  }
}
## Resultado despues de enviar SEND, devuelve una lista con todas las clinicas
{
  "data": {
    "allClinics": [
      {
        "idRuc": 10,
        "name": "Clinica1"
      }
    ]
  }
}
```

### Consultar todos los dentistas con relay
Consulta todos los dentistas con [relay](https://facebook.github.io/relay/), esta herramienta crea un ID unico con el cual se pueden jalar los objetos a la siguiente peticion, es interesante para su uso, asi que aqui vamos.
```
## Funciona en Graphql y Altair
## Primero haremos la consulta, la cual se escirbe algo diferente
query{
  allDentists{
    edges{
      node{
        id
        idDentist
        dentistUser {    ## Notaras que el dentisUser es un objeto
          id             ## que esta jalando los datos del model User
          username       ## esto es porque ambas tablas estan relacionadas
          email          ## esto esta en el archivo models.py
        }
        dentistCreate
      }
    }
  }
}
## Resultado despues de enviar SEND, devuelve una lista con todos los dentistas pero 
## con id, que es un codigo unico, del cual se puede jalar en la siguiente solicitud
## esto lo veremos mas adelante
{
  "data": {
    "allDentists": {
      "edges": [
        {
          "node": {
            "id": "RGVudGlzdE5vZGU6MQ==",   ## este es el codigo unico
            "idDentist": 1,
            "dentistUser": {
              "id": "2",
              "username": "dentista1",
              "email": ""
            },
            "dentistCreate": "2019-02-04T11:57:23+00:00"
          }
        }
      ]
    }
  }
}
```

### Consultar dentista usando relay
Tomaremos el id unico generado por relay del ejemplo anterior para la consulta del id_dentist
```
## Funciona en Graphql y Altair
query{
  dentist(id:"RGVudGlzdE5vZGU6MQ==")    ## este es el codigo unico anterior
  {
    idDentist
    dentistUser{
      username
      email
    }
    dentistCreate
  }
}
## despues de enviar el send, devuelve el dentista encontrado
{
  "data": {
    "dentist": {
      "idDentist": 1,
      "dentistUser": {
        "username": "dentista1",
        "email": ""
      },
      "dentistCreate": "2019-02-04T11:57:23+00:00"
    }
  }
}
```
El relay sirve puede ser una herramienta poderosa, ahi que saver usarla e implementarla.

## Herramientas para el futuro

### Colegiatura
* [Link-Colegio](http://www.col.org.pe/colegiados/s/?filter=27831&district=0&specialties=0) para hacer un raspado de los datos que estan accesibles 

### Boyager
Para ver las APIS, se deberia usar [Graphql-Boyager](https://github.com/APIs-guru/graphql-voyager) que es un visor para ver las apis.
Se podria usar por el momento el [demo](https://apis.guru/graphql-voyager/), copiando la [Introspection Schema Query.txt](https://github.com/techtipsworld/files/blob/master/graphql/Introspection%20Schema%20Query.txt), corriendolo en el [graphql IDE](127.0.0.1:8000/graphql), pegando el resultado en Custom Schema y precionar Change Schema, para mas detalle mira el [video](https://www.youtube.com/watch?v=7MTyR5L-xYY&t=47s&index=2&list=FLngXDbIEDnCqY6OHbaiep2g). **Mas adelante se podria instalar en la aplicacion**

Se debe buscar una plantilla de diseño o armarlo, aunque es mas trabajoso armarlo.

## Referencias

* [Ref1](https://medium.com/yld-engineering-blog/using-vue-with-apollo-65e2b1297592) - Integrando correctamente Vuejs y Graphql
* [Ref2](https://www.howtographql.com/vue-apollo/5-authentication/) - Para autenticar
* [Ref3](https://www.thepolyglotdeveloper.com/2019/01/query-graphql-api-vuejs-axios/)

## :skull: Autor :hamburger:

:beer: joejhona@gmail.com :pizza:
Disculpen por algunos errores ortográficos.