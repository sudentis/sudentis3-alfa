import graphene
import graphql_jwt

from graphene import relay, ObjectType
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import Dentist, Clinic, Position, Patient

from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
###############################################
## Usuario con filtro nombre, id y correo
class UserConsult(DjangoObjectType):
    class Meta:
        model       = get_user_model()
        only_fields = ('id','username','email')
###############################################
## Clinica con filtro de usuario
class PositionConsult(DjangoObjectType):
    class Meta:
        model       = Position
###############################################
## Clinica sin filtro
class ClinicType(DjangoObjectType):
    class Meta:
        model = Clinic
###############################################
## Dentista sin filtro
class DentistType(DjangoObjectType):
    class Meta:
        model = Dentist
###############################################
## Crear Usuario ##
### Modelo Usuario ###
class UserType(DjangoObjectType):
    class Meta:
        model   = get_user_model()
### Crear usuario ###
### Corregir al consultar ususario se ve todo, 
### solo se deve ver lo esencial, nombre y correo
class CreateUser(graphene.Mutation):
    user = graphene.Field(UserType)
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email    = graphene.String(required=True)
    def mutate(self, info, username, password, email):
        user = get_user_model()(
            username=username,
            email=email,
        )
        user.set_password(password)
        user.save()
        return CreateUser(user=user)
###############################################
## Crear Dentista ##
class CreateDentist(graphene.Mutation):
    user       = graphene.Field(UserType)
    id_dentist = graphene.Int()
    class Arguments:
        id_dentist = graphene.Int(required=True)
    def mutate(self, info, id_dentist):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Necesitas entrar para registrate como dentista')
        dentist_new = Dentist(id_dentist=id_dentist,dentist_user=user)
        dentist_new.save()
        return CreateDentist(id_dentist=dentist_new.id_dentist)
###############################################
## Crear Job ##
class CreateClinic(graphene.Mutation):
    user     = graphene.Field(UserType)
    job      = graphene.String()
    id_ruc   = graphene.Int()
    name     = graphene.String()
    username =graphene.String()
    class Arguments:
        id_ruc   = graphene.Int(required=True)
        name     = graphene.String(required=True)
    def mutate(self, info, id_ruc, name):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Necesitas entrar y registrate como dentista para crear tu clinica')
        #dentist = Dentist.objects.get(dentist_user=user)
        #if not dentist.DoesNotExist:
        #    raise Exception('Necesitas registrate como dentista')
        dentist = get_object_or_404(Dentist,dentist_user=user)
        idruc = Clinic.objects.filter(id_ruc=id_ruc).first()
        if idruc:
            raise Exception('El ruc ya esta registrado')
        clinic   = Clinic(name=name, id_ruc=id_ruc)
        clinic.save()
        position = Position(job='Administrador',dentist=dentist,clinicjob=clinic) 
        position.save()
        return CreateClinic(
            id_ruc=clinic.id_ruc,
            name=clinic.name,
            username=user.username,
            job=position.job
        )
###############################################
## Crear Dentista para Relay ##
class DentistNode(DjangoObjectType):
    class Meta:
        model = Dentist
        filter_fields = ['id_dentist']
        interfaces = (relay.Node, )
###############################################
## Consultar Paciente ##
class PatientType(DjangoObjectType):
    class Meta:
        model   = Patient
###############################################
## Crear Paciente ##
class AddPatient(graphene.Mutation):
    name    = graphene.String()
    id      = graphene.Int()
    class Arguments:
        name = graphene.String(required=True)
    def mutate(self, info, name):
        patient = Patient(name=name)
        patient.save()
        id = patient.pk
        return AddPatient(name=name,id=id)
###############################################
## Editar Paciente ##
class UpdatePatient(graphene.Mutation):
    name    = graphene.String()
    id      = graphene.Int()
    class Arguments:
        id   = graphene.Int(required=True)
        name = graphene.String(required=True)
    def mutate(self, info, id, name):
        patient = get_object_or_404(Patient,pk=id)
        patient.name = name
        patient.save()
        id = patient.pk
        return UpdatePatient(name=name,id=id)
###############################################
## Querys y Mutations ##
###############################################
class Query(object):
    #############################################
    ## Consultando todas las clinicas ##
    all_clinics = graphene.List(ClinicType)
    def resolve_all_clinics(self, info, **kwargs):
        return Clinic.objects.all()
    #############################################
    ## Consultando todos los usuarios ##
    all_users = graphene.List(UserConsult)
    def resolve_all_users(self, info, **kwargs):
        return get_user_model().objects.all()
    #############################################
    ## Consultando mis datos ##
    me  = graphene.Field(UserConsult)
    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')
        return user
    #############################################
    ## Consultando clinica ##
    meposition = graphene.Field(PositionConsult)
    def resolve_meposition(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')
        return Position.objects.get(dentist=user.dentist)

    #############################################
    ## Consulta todos los dentistas con relay ##
    all_dentists= DjangoFilterConnectionField(DentistNode)
    #############################################
    ## Consulta dentista con relay ##
    dentistn    = relay.Node.Field(DentistNode)
    #############################################
    ## Consulta el usuario ##
    user    = graphene.Field(UserType,
                id=graphene.Int(),
                email=graphene.String(),
                username=graphene.String())
    def resolve_user(self, info, **kwargs):
        id       = kwargs.get('id')
        email    = kwargs.get('email')
        username = kwargs.get('username')
        if id is not None:
            return get_user_model().objects.get(pk=id)
        if email is not None:
            return get_user_model().objects.get(email=email)
        if username is not None:
            return get_user_model().objects.get(username=username)
        return None
    #############################################
    ## Consulta el dentista ##
    dentist = graphene.Field(DentistType,
                id=graphene.Int())
    def resolve_dentist(self, info, **kwargs):
        id  = kwargs.get('id')
        if id is not None:
            return Dentist.objects.get(pk=id)
        return None
    #############################################
    ## Consulta pacientes ##
    patients = graphene.List(PatientType)
    def resolve_patients(self, info, **kwargs):
        return Patient.objects.all()
###############################################
class Mutation(graphene.ObjectType):
    #############################################
    ## Crear Usuario ##
    create_user = CreateUser.Field()
    #############################################
    ## Crear Dentista ##
    create_dentist = CreateDentist.Field()
    #############################################
    ## Crear Clinica ##
    create_clinic = CreateClinic.Field()
    #############################################
    ## Crear Paciente ##
    add_patient = AddPatient.Field()
    #############################################
    ## Editar Paciente ##
    update_patient = UpdatePatient.Field()


