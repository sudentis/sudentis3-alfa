import datetime
from django.db import models
from django.conf import settings

# Create your models here.

# Create your models here.
class Dentist(models.Model):
    id_dentist      = models.PositiveIntegerField('DNI:', primary_key=True)
    dentist_create  = models.DateTimeField(default=datetime.datetime.now)
    dentist_user    = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.dentist_user.username

class Clinic(models.Model):
    id_ruc  = models.PositiveIntegerField('RUC:', primary_key=True)
    name    = models.CharField(max_length=20)

    def __str__(self):
        return self.name

class Position(models.Model):
    job        = models.CharField(max_length=20)
    dentist    = models.ForeignKey(Dentist, related_name='jobs', on_delete=models.CASCADE)
    clinicjob  = models.ForeignKey(Clinic, related_name='clinicjobs',on_delete=models.CASCADE)

    def __str__(self):
        return self.job

class Patient(models.Model):
    name    = models.CharField(max_length=40)
    #clinic  = models.ForeignKey(Clinic, related_name='clinics', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class History(models.Model): 
    history_num         = models.AutoField(primary_key=True)
    address             = models.CharField(max_length=40)
    history_patient     = models.ForeignKey(Patient, related_name='patients',on_delete=models.CASCADE) 

    def __str__(self):
        return self.address

class Attention(models.Model):
    text      = models.CharField(max_length=100)
    history   = models.ForeignKey(History, related_name='historys', on_delete=models.CASCADE)

    def __str__(self):
        return self.text

class Tryst(models.Model):
    turno     = models.CharField(max_length=50)
    attention = models.ForeignKey(Attention, related_name='attentions', on_delete=models.CASCADE)

    def __str__(self):
        return self.turno
