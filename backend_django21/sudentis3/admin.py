from django.contrib import admin
from .models import Dentist, Position, Clinic, Patient, History, Attention, Tryst

admin.site.register(Dentist)
admin.site.register(Position)
admin.site.register(Clinic)
admin.site.register(Patient)
admin.site.register(History)
admin.site.register(Attention)
admin.site.register(Tryst)